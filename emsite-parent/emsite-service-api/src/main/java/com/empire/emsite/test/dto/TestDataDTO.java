package com.empire.emsite.test.dto;

import java.util.Date;

import com.empire.emsite.common.dto.BaseDataDTO;

/**
 * 类TestDataDTO.java的实现描述：TestDataDTO数据传输
 * 
 * @author arron 2018年3月5日 下午6:05:22
 */
public class TestDataDTO extends BaseDataDTO {
    private static final long serialVersionUID = 1L;
    private String            userId;               // 归属用户id
    private String            officeId;             // 归属部门id
    private String            areaId;               // 归属区域id
    private String            userName;             // 归属用户
    private String            officeName;           // 归属部门
    private String            areaName;             // 归属区域
    private String            name;                 // 名称
    private String            sex;                  // 性别
    private Date              inDate;               // 加入日期
    private Date              beginInDate;          // 开始 加入日期
    private Date              endInDate;            // 结束 加入日期

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOfficeId() {
        return officeId;
    }

    public void setOfficeId(String officeId) {
        this.officeId = officeId;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getOfficeName() {
        return officeName;
    }

    public void setOfficeName(String officeName) {
        this.officeName = officeName;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Date getInDate() {
        return inDate;
    }

    public void setInDate(Date inDate) {
        this.inDate = inDate;
    }

    public Date getBeginInDate() {
        return beginInDate;
    }

    public void setBeginInDate(Date beginInDate) {
        this.beginInDate = beginInDate;
    }

    public Date getEndInDate() {
        return endInDate;
    }

    public void setEndInDate(Date endInDate) {
        this.endInDate = endInDate;
    }

}
