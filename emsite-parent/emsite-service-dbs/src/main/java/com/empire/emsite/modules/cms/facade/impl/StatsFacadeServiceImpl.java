/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.modules.cms.facade.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.empire.emsite.modules.cms.entity.Category;
import com.empire.emsite.modules.cms.facade.StatsFacadeService;
import com.empire.emsite.modules.cms.service.StatsService;

/**
 * 类StatsFacadeServiceImpl.java的实现描述：统计FacadeService接口实现类
 * 
 * @author arron 2017年9月17日 下午9:46:12
 */
@Service
public class StatsFacadeServiceImpl implements StatsFacadeService {
    @Autowired
    private StatsService statsService;

    @Override
    public List<Category> article(Map<String, Object> paramMap, String siteId) {
        return statsService.article(paramMap, siteId);
    }
}
