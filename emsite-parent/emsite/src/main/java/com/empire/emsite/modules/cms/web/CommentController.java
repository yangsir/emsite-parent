/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.modules.cms.web;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.empire.emsite.common.persistence.Page;
import com.empire.emsite.common.utils.StringUtils;
import com.empire.emsite.common.web.BaseController;
import com.empire.emsite.modules.cms.entity.Comment;
import com.empire.emsite.modules.cms.facade.CommentFacadeService;
import com.empire.emsite.modules.sys.utils.DictUtils;
import com.empire.emsite.modules.sys.utils.UserUtils;

/**
 * 类CommentController.java的实现描述：评论Controller
 * 
 * @author arron 2017年10月30日 下午7:11:19
 */
@Controller
@RequestMapping(value = "${adminPath}/cms/comment")
public class CommentController extends BaseController {

    @Autowired
    private CommentFacadeService commentFacadeService;

    @ModelAttribute
    public Comment get(@RequestParam(required = false) String id) {
        if (StringUtils.isNotBlank(id)) {
            return commentFacadeService.get(id);
        } else {
            return new Comment();
        }
    }

    @RequiresPermissions("cms:comment:view")
    @RequestMapping(value = { "list", "" })
    public String list(Comment comment, HttpServletRequest request, HttpServletResponse response, Model model) {
        comment.setCurrentUser(UserUtils.getUser());
        Page<Comment> page = commentFacadeService.findPage(new Page<Comment>(request, response), comment);
        model.addAttribute("page", page);
        return "modules/cms/commentList";
    }

    @RequiresPermissions("cms:comment:edit")
    @RequestMapping(value = "save")
    public String save(Comment comment, RedirectAttributes redirectAttributes) {
        if (beanValidator(redirectAttributes, comment)) {
            if (comment.getAuditUser() == null) {
                comment.setAuditUser(UserUtils.getUser());
                comment.setAuditDate(new Date());
            }
            comment.setDelFlag(Comment.DEL_FLAG_NORMAL);
            comment.setCurrentUser(UserUtils.getUser());
            comment.setCreateDate(new Date());
            commentFacadeService.save(comment);
            addMessage(redirectAttributes, DictUtils.getDictLabel(comment.getDelFlag(), "cms_del_flag", "保存") + "评论'"
                    + StringUtils.abbr(StringUtils.replaceHtml(comment.getContent()), 50) + "'成功");
        }
        return "redirect:" + adminPath + "/cms/comment/?repage&delFlag=2";
    }

    @RequiresPermissions("cms:comment:edit")
    @RequestMapping(value = "delete")
    public String delete(Comment comment, @RequestParam(required = false) Boolean isRe,
                         RedirectAttributes redirectAttributes) {
        commentFacadeService.delete(comment, isRe);
        addMessage(redirectAttributes, (isRe != null && isRe ? "恢复审核" : "删除") + "评论成功");
        return "redirect:" + adminPath + "/cms/comment/?repage&delFlag=2";
    }

}
