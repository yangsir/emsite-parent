/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.modules.cms.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.empire.emsite.common.mapper.JsonMapper;
import com.empire.emsite.common.persistence.Page;
import com.empire.emsite.common.utils.StringUtils;
import com.empire.emsite.common.web.BaseController;
import com.empire.emsite.modules.cms.entity.Category;
import com.empire.emsite.modules.cms.entity.Link;
import com.empire.emsite.modules.cms.facade.CategoryFacadeService;
import com.empire.emsite.modules.cms.facade.LinkFacadeService;
import com.empire.emsite.modules.cms.utils.CmsUtils;
import com.empire.emsite.modules.sys.utils.UserUtils;

/**
 * 类LinkController.java的实现描述：链接Controller
 * 
 * @author arron 2017年10月30日 下午7:12:00
 */
@Controller
@RequestMapping(value = "${adminPath}/cms/link")
public class LinkController extends BaseController {

    @Autowired
    private LinkFacadeService     linkFacadeService;
    @Autowired
    private CategoryFacadeService categoryFacadeService;

    @ModelAttribute
    public Link get(@RequestParam(required = false) String id) {
        if (StringUtils.isNotBlank(id)) {
            return linkFacadeService.get(id);
        } else {
            return new Link();
        }
    }

    @RequiresPermissions("cms:link:view")
    @RequestMapping(value = { "list", "" })
    public String list(Link link, HttpServletRequest request, HttpServletResponse response, Model model) {
        //		User user = UserUtils.getUser();
        //		if (!user.isAdmin() && !SecurityUtils.getSubject().isPermitted("cms:link:audit")){
        //			link.setUser(user);
        //		}
        link.setCurrentUser(UserUtils.getUser());
        Page<Link> page = linkFacadeService.findPage(new Page<Link>(request, response), link, true);
        model.addAttribute("page", page);
        return "modules/cms/linkList";
    }

    @RequiresPermissions("cms:link:view")
    @RequestMapping(value = "form")
    public String form(Link link, Model model) {
        // 如果当前传参有子节点，则选择取消传参选择
        if (link.getCategory() != null && StringUtils.isNotBlank(link.getCategory().getId())) {
            List<Category> list = categoryFacadeService.findByParentId(link.getCategory().getId(),
                    CmsUtils.getCurrentSiteId());
            if (list.size() > 0) {
                link.setCategory(null);
            } else {
                link.setCategory(categoryFacadeService.get(link.getCategory().getId()));
            }
        }
        model.addAttribute("link", link);
        return "modules/cms/linkForm";
    }

    @RequiresPermissions("cms:link:edit")
    @RequestMapping(value = "save")
    public String save(Link link, Model model, RedirectAttributes redirectAttributes) {
        if (!beanValidator(model, link)) {
            return form(link, model);
        }
        link.setCurrentUser(UserUtils.getUser());
        linkFacadeService.save(link);
        addMessage(redirectAttributes, "保存链接'" + StringUtils.abbr(link.getTitle(), 50) + "'成功");
        return "redirect:" + adminPath + "/cms/link/?repage&category.id=" + link.getCategory().getId();
    }

    @RequiresPermissions("cms:link:edit")
    @RequestMapping(value = "delete")
    public String delete(Link link, String categoryId, @RequestParam(required = false) Boolean isRe,
                         RedirectAttributes redirectAttributes) {
        linkFacadeService.delete(link, isRe);
        addMessage(redirectAttributes, (isRe != null && isRe ? "发布" : "删除") + "链接成功");
        return "redirect:" + adminPath + "/cms/link/?repage&category.id=" + categoryId;
    }

    /**
     * 链接选择列表
     */
    @RequiresPermissions("cms:link:view")
    @RequestMapping(value = "selectList")
    public String selectList(Link link, HttpServletRequest request, HttpServletResponse response, Model model) {
        link.setCurrentUser(UserUtils.getUser());
        list(link, request, response, model);
        return "modules/cms/linkSelectList";
    }

    /**
     * 通过编号获取链接名称
     */
    @RequiresPermissions("cms:link:view")
    @ResponseBody
    @RequestMapping(value = "findByIds")
    public String findByIds(String ids) {
        List<Object[]> list = linkFacadeService.findByIds(ids);
        return JsonMapper.nonDefaultMapper().toJson(list);
    }
}
